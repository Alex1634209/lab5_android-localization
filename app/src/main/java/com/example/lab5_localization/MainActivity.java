package com.example.lab5_localization;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.content.Context;
import android.widget.ImageButton;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    ImageButton imButton;
    private static final String TAG="buttonclick";
    Context context = this;
    CharSequence text = "Button was clicked";
    int duration = Toast.LENGTH_SHORT;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        imButton = (ImageButton)findViewById(R.id.imageButton);
        imButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v){
               Toast toast = Toast.makeText(context,R.string.toast,duration);
               toast.show();
            }
        });

    }
}
